# Application du gesion des rétrospectives (partie back-end)

Il s'agit d'une applications qui sert à animer la réunion de fin du chaque sprint 

# La création de base des données 

Tout d'abord, si vous avez déja cloné le repo App_Gestion_Retrospective, faites un git pull pour récupérer la projet 

si non, faites un git clone 

après que vous clonez le projet, vous devez installer symfony.

vous trouvez dans le lien suivant toutes les explications: https://symfony.com/doc/current/setup.html#setting-up-an-existing-symfony-project

Ensuite, Dans le fichier .env ,Vous devez mettre les informations de connexion à la base des données dans la variable d'environnemment  DATABASE_URL

jeuste après, dans la terminal, vous lancez la ligne de commande suivante pour faire la migration: 
    
    php bin/console doctrine:migrations:migrate

Les fonctionnalités faites :
--------------------------

- Ajouter un feedback en format json :

    {
        content : "",
        status : ""
    }

- Afficher les feedbacks de toute les semaines 
- Afficher les feedbacks selon la semaine en envoyer une requête GET avec un paramètre en format suivante :

    week_year=W-Y 

- Mettre en place le formulaire d'inscription, les données doivent être envoyer en format json :

        {
            "username": "bobo",
             "email": "bobp@mimo.com",
             "plainPassword":{
                 "first": "momo",
                 "second": "momo"
           } 
        }

- Récupérer la liste des participants et des non-participants 



L'API Rest : 
----------
L'API client se situe entre le site web et le service web. Sa fonction principale est de transmettre les informations de l'application sur le site web, de telle sorte que l'utilisateur puisse les modifier à distance. Cette API est privée et uniquement accessible par un client authentifié.

C'est une API  pouvant utiliser Json comme format de données.

Voici une liste rapide des actions possibles, avec leur URL RESTfull, les verbes HTTP utilisables et les formats servis.

|  La Tâche |    URL  |   Verbes |   format servis | 
|:--------  |:------: |:-------: |---------------: |
|  ajouter un feedback   | feedbacks/create     | POST |   JSON |
| récupérer tous les feedbacks | feedbacks/index | GET | JSON | 
| récupérer les feedbacks selon la semaine | feedbacks/currentweek avec un paramtère sous forme : week_year=W-Y | GET | JSON| 
| s'inscrire | register | POST | JSON | 
| récupérer la liste des participants | admin/contributuion | GET | JSON | 
| récupérer la liste des non-participants| admin/users-not-participated | GET | JSON | 



Les tâches restes :
-------------------

- Ajouter l'option de se connecter et se déconnecter.
- Récupérer l'id de utilisateur connecté pour mettre à jour la date de sa dernière participation
- limiter l'accés aux listes des participants juste pour les admis 
- limiter l'utilisatu-ion de l'application juste pour les utilisateurs connectés 
