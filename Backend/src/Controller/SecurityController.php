<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\SecurityType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractFOSRestController
{
    

    /**
     * @Route("/register", name="register", methods={"POST"})
     */
    public function register (Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        // il faut envoyer les données en format json, exemple:
        // {
        //     "username": "bobo",
        //     "email": "bobp@mimo.com",
        //     "plainPassword":{
        //         "first": "momo",
        //         "second": "momo"
        //     } 
        // }
        $user = new Users();
        $form = $this->createForm(SecurityType::class, $user);
        $data = json_decode($request->getContent(), true); 
        
        $week_current =date("W"."-"."Y") ;
        $user->setWeekYear($week_current);
        $form->submit($data);
        
        if($form->isSubmitted() && $form->isValid()){ 
            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPlainPassword()));
            // insérer user dans la base des données  
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            // view
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));  
        }
        return $this->handleView($this->view($form->getErrors()));
        
    }
    
    
}
