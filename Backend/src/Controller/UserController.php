<?php

namespace App\Controller;

use App\Entity\Users;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractFOSRestController
{
    /**
     * @Route("/admin/contribution", name="user")
     */
    public function participants_of_week()
    {
        // autoriser que pour les admin
        $week_current =date("W"."-"."Y");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT u.username FROM App\Entity\Users u WHERE u.week_year = :week ")->setParameter('week', $week_current);
        $users_participated  = $query->getResult();
        
        return $this->handleView($this->view(['usersList' => $users_participated ])) ;
        
    }
    
    // afficher les utilisateurs qui sont pas encore participés 
    /**
     * @Route("/admin/users_not_participated", name="list_users_not_participated")
     */
    public function users_not_participated()
    {
        // autoriser que pour les admin
        $week_current =date("W"."-"."Y");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT u.username FROM App\Entity\Users u WHERE u.week_year != :week ")->setParameter('week', $week_current);
        $users_not_participated = $query->getResult();
        
        return $this->handleView($this->view(['usersList' => $users_not_participated ])) ;
        
    }
}
