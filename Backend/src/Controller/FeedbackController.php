<?php

namespace App\Controller;

use App\Entity\Feedbacks;
use App\Entity\Users;
use App\Form\FeedbackFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;



class FeedbackController extends AbstractFOSRestController
{
    
    /**
     * @Route("/feedbacks/create", name="feedback", methods={"POST"})
     */
    public function add_feedback(Request $request): Response 
    {
        // autoriser pour les utilisateurs connectés
        $feedback = new Feedbacks(); 
        $form = $this->createForm(FeedbackFormType::class, $feedback);
        $data = json_decode($request->getContent(), true); 
        $week_current =date("W"."-"."Y") ;
        $feedback->setWeekYear($week_current);
        $form->submit($data);

        // mettre à jour le week_year de l'utilisateur en ajoutant un feedback
        // selectionner l'utilisateur 
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(Users::class)->find('2');
        
        if($form->isSubmitted() && $form->isValid()){ 
            // insérer le feedback dans la base des données  
            $em = $this->getDoctrine()->getManager();
            $em->persist($feedback);
            $em->flush();
            // mettre à jour la date de dernière feedback (semaine/année)
            $user->setWeekYear($week_current);
            $entityManager->flush();
            // view
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));  
        }
        return $this->handleView($this->view($form->getErrors()));

    }
    /**
     * @Route("/feedbacks/index", name="feedbacks_list", methods={"GET"})
     */
    public function get_feedbacks()
    {
        // autoriser pour les utilisateurs connectés 
        $feedbacks_list =$this->getDoctrine()->getRepository(Feedbacks::class)->findAll() ; 
        return $this->handleView($this->view(['feedbacksList' => $feedbacks_list])) ;

    }
    /**
     * @Route("/feedbacks/currentweek", name="feedbacks_list_of_week", methods={"GET"})
     */
    public function feedbacks_by_week(Request $request)
    {
        // autoriser pour les utilisateurs connectés ( à ajouter )
        // -------request param week_year = W-Y
        $week = $request->query->get('week_year');
        $feedbacks_list =$this->getDoctrine()->getRepository(Feedbacks::class)->findBy(['week_year' => $week]) ; 
        
        return $this->handleView($this->view(['feedbacksList' => $feedbacks_list])) ;

    }


}
     