
import '../styles/App.css';
import Register from './pages/Register';
import Login from './pages/Login';
import AddFeedback from './pages/AddFeedback';
import ReadFeedbacks from './pages/ReadFeedbacks';
import ListParticipants from './pages/ListParticipants';
import { BrowserRouter, Route, Switch} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route path="/" exact component={Register}/>
      <Route path="/login" component={Login}/>
      <Route path="/feedbacks/index" component={ReadFeedbacks}/>
      <Route path="/feedbacks/create" component={AddFeedback}/>
      <Route path="/admin/contribution" component={ListParticipants}/>
    </Switch>
      
    </BrowserRouter>
    
  );
}

export default App;


