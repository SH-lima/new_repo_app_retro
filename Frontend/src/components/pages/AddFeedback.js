import Banner from '../Banner';
import React from 'react';
import '../../styles/App.css';

class AddFeedbackForm extends React.Component{
    constructor(props){
       super(props);
       this.state = {
        feedbacks:{},
        errors:{}
       };
    }
    handleValidation(){
        let feedbacks = this.state.feedbacks;
        let errors = {}
        let formIsValid = true;
        
        this.setState({errors : errors});
        return formIsValid;
    }
    handleSubmit(event){
        this.handleChange(event)
        // event.preventDefault();  
        if(this.handleValidation()){
         alert('Les données ont été soumises ');   
        }else{
        alert('il y a des erreurs ')
        }
    }
    handleChange(event, feedback){
        let feedbacks = this.state.feedbacks;
            feedbacks[feedback]= event.target.value;
            this.setState({feedbacks}) ;     
        
    } 
    render(){
        return (
            <form method="POST">
                <div>
                    <label>Ajouter un feedback positif </label>
                    <input type="text" value={this.state.feedbacks["content"]} onChange={event =>this.handleChange(event, "content")} ref="content" />
                    <input type="hidden" value="like" onChange={event =>this.handleChange(event, "status")} ref="status" />
                </div>
                <div>
                    <label>Ajouter un feedback négatif </label>
                    <input type="text"/>
                    <input type="hidden"/>
                </div>
                <input type="submit" value="Envoyer"/>
            </form>
        )
    }
    
}

function AddFeedback(){

    return(
    <>
        <Banner/>
        <AddFeedbackForm/>
    </>
    );
};

export default AddFeedback;