import Banner from '../Banner';
import React from 'react';
import '../../styles/App.css';

class Feedbacks extends React.Component{
constructor(props){
    super(props);
    this.state = {
        allFeedbacks : []
    }
}
componentDidMount() {
    // Simple POST request with a JSON body using fetch
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
        // body: JSON.stringify({ title: 'React POST Request Example' })
    };
    fetch('http://127.0.0.1:8000/feedbacks/index')
        .then(response => response.json())
        .then(data => this.setState({ allFeedbacks : data.id }));
}
render(){
    return(
        <div>
            {this.state.allFeedbacks["data"]}
        </div>
    )
}
}

function ReadFeedbacks(){

    return(
    <>
        <Banner/>
        <Feedbacks/>
    </>
    );
};

export default ReadFeedbacks;