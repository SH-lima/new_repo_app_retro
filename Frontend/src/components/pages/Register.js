import Banner from '../Banner';
import React from 'react';
import '../../styles/App.css';

class RegisterForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            fields : {},
            errors : {}

        };
        this.handleSubmit = this.handleSubmit.bind(this)

    }
    
    handleValidation(){
        let fields = this.state.fields;
        let errors = {}
        let formIsValid = true;
        if(!fields["name"]){
            formIsValid = false;
            errors["name"]="cannot be empty"
        }
        if(!fields["password"]){
            formIsValid = false;
            errors["password"]="cannot be empty"
        }
        if(!fields["passwordConf"]){
            formIsValid = false;
            errors["passwordConf"]="cannot be empty"
        }
        if(fields["password"] !== fields["passwordConf"]){
            formIsValid = false;
            errors["errorPassword"]="il faut mettre le même mot de passe "
        }
        this.setState({errors : errors});
        return formIsValid;
    }
    handleSubmit(event){
        this.handleChange(event)
        // event.preventDefault();  
        if(this.handleValidation()){
         alert('Les données ont été soumises ');   
        }else{
        alert('il y a des erreurs ')
        }
    }
    handleChange(event, field){
        let fields = this.state.fields;
            fields[field]= event.target.value;
            this.setState({fields}) ;     
        
    } 
    
    render(){
        return (
            <form className="form" onSubmit={this.handleSubmit}>
                <input type="text" placeholder="Identifiant" value={this.state.fields["name"]} onChange={event =>this.handleChange(event, "name")} ref="name"/>
                <span>{this.state.errors["name"]}</span>
                <input type="password" placeholder="Mot de passe" value={this.state.fields["password"]} onChange={event =>this.handleChange(event, "password")} ref="password"/>
                <span>{this.state.errors["password"]}</span>
                <input type="password" placeholder="Confirmation du mot de passe" value={this.state.fields["passwordConf"]} onChange={event =>this.handleChange(event, "passwordConf")} ref="passwordConf"/>
                <span>{this.state.errors["passwordConf"]}</span>
                <br/>
                <span>{this.state.errors["errorPassword"]}</span>
                <input type="submit" value="Envoyer"/> 
            </form>
        );
    }
}



function Register (){

    return(
    <>
        <Banner/>
        <div classNameName="Register">
            <div className="sentence">
                <h3 className="rotation">Tu</h3>
                <h3>veux</h3>
                <h3 className="rotation">rejoindre</h3>
                <h3>notre</h3>
                <h3 className="rotation">communauté</h3>
            </div>
            <div className="sentence">
                <h3>inscris-toi</h3>
                <h3 className="rotation">maintenant</h3>
            </div>
        </div>
        <RegisterForm className="form "/>
        <a href="/login">T'as déjà un compte?</a>
    </>
    );
};

export default Register;