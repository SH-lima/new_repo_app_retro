# App_Gestion_Retrospective


 Rétroconception en UML 
=====================

Le projet est de créer une application qui permet aux formateurs de obtenir les avis de tous les apprenants d'une formation chaque semaine. Ce document présente :
    
- La demande initiale 
- Le travail qui était fait pendant les deux premiers sprints
- Bugs connus
- Le travail reste 

La demande initiale :
--------------------
Vous trouverez sur le lien suivant [le cahier des charges](https://gitlab.com/Glandalf/cda2021-resources/-/blob/master/sprint-01/brief.md) la demande initiale en détais 

Mais pour résumer, ce usecase diagramme présente les fonctionnalités de l'application :

![Usecase diagram](http://www.plantuml.com/plantuml/png/NOvDJWGX48NtdA8oUnREZojZ9l4C7k09rJsLeO50iLBlRcEmqyxkKU_xoBjMHT6s11DuKj94HUQRafV2JYL50wUfKAjS29NQUCCl4li19iFTCJCXvo0ENIBwDDGTXyfqA3nBLIw7Bho_FUsxa6Q9epZk2tXFe8dPVy3T1t7w_oqywOqtg0xglATSYhK5_GN3EeFIH355cr5Kd6H4_VCk3yDIqz4ycs_JoDhNFdzBnosTjdGUgJi68rxMuykNjGHpvUZR4du0) 

* L'utilisateur peut s'inscrire, connecter à l'application
      
* Une fois l'utilisateur est connecté, il peux ajouter au moins un feedback Dislike et 
    
    un feedback like

* Il peut lire tous les feedbacks rédigés pendant la semaine 

* Pour les admins, ils pouvent voir la liste des pesonnes qui n'ont pas participé 

Le travail qui était fait (partie back uniquement):
--------------------------

![Sequence diagram](http://www.plantuml.com/plantuml/png/VP9HJiCm38RVVOh_0boWKj6AU4T2uW0cSQk8B2rnChJRauPrmB8j3v6g_FhysUvE4yUKzvxOf3dYJIM25GCD3uyObiKxmydD0HsYJ4vJh9z4VzzAzFcf0rkB85_81T84XWv-dYQnH7sFbrVqFLqMgH7Nm5VsGQ8RZoSq-mJDneZgc3qCgn01A1Pej0_idQslzRhKmkQ5tymKvxpKMPpV2FKC3RzsFOhOTpOVMwmP3ZguK7W9t8GRwMvh3dkTS9qvu5_gZbyKNWLjhQc4EufHFhDeEXigRZgkvINODdfjmDRfulbuYaYmHE9LCBBpI3FC78AOXDjB37FwMIJkRZAATR6GLcZzbR3m95lBE1MWSbQ9NRdotdy3)

ce diagram présente les tâches qui ont été réalisées :

## L'inscription : 

pour un utilisateur qui utilise l'application pour la première fois, il faut 
    
s'inscrire, une fois la demande accepté et les données de l'utilisateur sont stocké 
    
dans la base des données, l'utilisateur sera connecté à l'application directement  

## La connexion :

Si l'utilisateur a déjà inscrire, il se connecte à l'application en mettant son 
    
identifiant et son mot de passe, si ils correspondent aux données stockés dans la base 
    
des données, l'utilisateur est connecté 

sinon, sa demande est rejetée et un message de erreur s'affiche 

## L'utilisateur connecté :

en tant que utilisateur connecté, il peut ajouter minimum un feedback like et un 

feedback dislike, il peut également lire les feedbacks rédigés selon semaine. 

Bugs connus :
-------------
* Il manque l'affichage d'un message d'erreur lorsque l'utilisateur essaye d'envoyer
    
qu'un seul feedback like ou dislike 

* Il manque la possibilité de revenir sur la page de l'ajout un feedback lorsque 
    
l'utilisateur est sur la page des feedbacks 

Le travail reste :
------------------
* Ajouter une demande de confirmation avant d'envoyer les feedbacks

 ![Confirmation](http://www.plantuml.com/plantuml/png/VL51RWCX3BplAqQvzmDxg9AgN-W3A7OG3GlKXaD_tzqWLjkeX0D2PcOy7f-r62br2PEJL3C-b0KXESU4QSBzNF3oYalEmLjJV8gOOOZmLRKWHSQLcJwDlI7u6oC9oElsxFmZTLF23E5hLNxUu47Nn4FbeKFpVa99T-AfrGUzROfNBqkh2NzNrj9c7PZf3DvH76amtFiRPlYeB0Lczzl1w-zVyHbQhMLLBEhmlsRBYxomBIXf5rLau_ZPbeJDbXar-8InvPx3GNpe6JEK8nsXuqMJrnpCpmhfkPtNgoxX5m00)

* Ajouter la possibilité de voir la liste de non-participants, pour les admins 
